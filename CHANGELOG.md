# Change Log
All notable changes to the "cdt" extension will be documented in this file.

## [0.0.2]

Snippets for:

- w6form ($scope):
  - on(Apply|OK|Cancel)Clicked
  - before(Apply|OK|Cancel)Clicked
  - set(Apply|OK|Cancel)Text
  - set(Property|Tab)Visibility
  - showUserMessage
  - onDestroy
- w6serverServices:
  - countObjects
  - createObject
  - getObject
  - getObjects
  - getUser
  - sendSXP
  - updateObject
  - updateObjects
- w6dialogService:
  - error, question, show, warning
  - dialogButton

## [0.0.1]

Snippets for: 

- NotifyPropertyChanged
- Watch
- Custom panels