# Click Development Templates (cdt)

This is an extension aimed at providing easy to use (and understand) templates for click development.

## Features

| Prefix                | Context          | Action |
| --------------------- | ---------------- | ------------------------------------------------------------------------------------ |
| customPanel           | html, javascript | Generates a skeleton for custom panels.                                              | 
| watch                 | javascript       | Generates a function called everytime a specific property changes.                   |
| notifyPropertyChanged | javascript       | Generates a function that will mark a property as dirty (changed) in the Click form. |
| onFormButtonClicked | javascript | Generates a function called when (after) one of the OK, Apply or Cancel button is clicked. |
| beforeFormButtonClicked | javascript | Generates a function called before the 'onClick' functions of the OK and Apply buttons. |
| setFormButtonText | javascript | Sets the text of the Apply, OK or Cancel buttons. |
| setVisibility | javascript | Sets the visibility of Properties or Tabs on the form. |
| showUserMessage | javascript | Shows a message to the user near the top of the form. |
| onDestroy | javascript | Generates a function called when the form or its children is destroyed.| 
| countObjects | javascript | Generates a function that counts objects. |
| createObject | javascript | Generates a function that creates an object. |
| getObject | javascript | Generates a function that gets an object from the server. |
| getObjects | javascript | Generates a function that gets one or more objects from the server. |
| getUser | javascript | Generates a function that gets the current logged in user. |
| sendSXP | javascript | Generates a function that sends a SXP to the server. |
| updateObject | javascript | Generates a function that updates a specific object. |
| updateObjects | javascript | Generates a function that updates a list of objects. |
| dialog | javascript | Generates a function that creates dialog boxes. |
| dialogButton | javascript | Generates a dialog button object. |

## Requirements

VSCode, keyboard, hands or, alternatively, a butler to type for you.

## Installation

1. Get latest .vsix or, alterantively, build the project with `vsce package` (install with `npm i -g vsce`).
2. Open VS Code.
3. Press `Ctrl-Shift-P` to open the command menu
4. Type 'vsix', select the option "Extensions: Install from vsix", and open the .vsix file.

## Known Issues

1. Not
2. Enough
3. Templates

## Release Notes

## [0.0.2]

Snippets for:

- w6form ($scope):
  - on(Apply|OK|Cancel)Clicked
  - before(Apply|OK|Cancel)Clicked
  - set(Apply|OK|Cancel)Text
  - set(Property|Tab)Visibility
  - showUserMessage
  - onDestroy
- w6serverServices:
  - countObjects
  - createObject
  - getObject
  - getObjects
  - getUser
  - sendSXP
  - updateObject
  - updateObjects
- w6dialogService:
  - error, question, show, warning
  - dialogButton

### 0.0.1 

Snippets for: 

- NotifyPropertyChanged
- Watch
- Custom panels